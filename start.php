<?php
/**
 * A Frequently Asked Question Plugin
 *
 * @package SimpleFAQ
 */

elgg_register_event_handler('init', 'system', 'simple_faq_init');

/**
 * Simple FAQ plugin initialization functions.
 */
function simple_faq_init() {

	// Footer navigation
	$item = new ElggMenuItem('faq', elgg_echo('faq'), 'faq');
	elgg_register_menu_item('footer', $item);

	// Register a page handler, so we can have nice URLs
	elgg_register_page_handler('faq', 'simple_faq_page_handler');
	
	// register the faqs' JavaScript
	$simple_faq_js = elgg_get_simplecache_url('js', 'simple_faq/toc');
	elgg_register_simplecache_view('js/simple_faq/toc');
	elgg_register_js('elgg.simple_faq', $simple_faq_js);

}

function simple_faq_page_handler($page) {
	elgg_load_js('elgg.simple_faq');
	expages_page_handler($page, 'faq');
}
