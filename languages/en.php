<?php
/**
 * FAQ pages English language file
 */

$english = array(

	/**
	 * Menu items and titles
	 */
	'faq' => "FAQ",
	'expages:faq' => "Frequently Asked Questions",
);

add_translation("en", $english);
