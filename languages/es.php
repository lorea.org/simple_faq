<?php
/**
 * FAQ pages Spanish language file
 */

$spanish = array(

	/**
	 * Menu items and titles
	 */
	'faq' => "Preguntas frequentes",
	'expages:faq' => "Preguntas frequentes",
);

add_translation("es", $spanish);
