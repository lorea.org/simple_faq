<?php
/**
 * Show the FAQs' Table of Contents
 *
 * @package SimpleFAQ
 */

include(elgg_get_plugins_path() . 'simple_faq/vendors/jquery-stoc/jquery.stoc.js');

?>

elgg.provide('elgg.simple_faq');

elgg.simple_faq.init = function() {
	$(function(){
		$("<div class="elgg-simplefaq-toc"></div>").prependTo(".elgg-main .elgg-output").stoc({
			search: ".elgg-main .elgg-output",
			stocTitle: "",
			listType: "ol",
		});
	});
};

elgg.register_hook_handler('init', 'system', elgg.simple_faq.init);
